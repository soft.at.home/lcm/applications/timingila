/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <debug/sahtrace.h>
#include <uuid/uuid.h>

#include "timingila/timingila_defines.h"

#include "softwaremodules_priv.h"
#include "timingila_priv.h"

#define ME "softwaremodules_helper"
#define RPC "rpc"

amxd_object_t* get_du_obj_from_keys(amxd_dm_t* dm, amxc_var_t* args) {
    amxd_object_t* ret = NULL;
    const char* duid = NULL;

    duid = GET_CHAR(args, SM_DM_DU_DUID);
    if(duid) {
        ret = amxd_dm_findf(dm, SOFTWAREMODULES_DM_DU ".[" SM_DM_DU_DUID " == '%s'].", duid);
    } else {
        SAH_TRACEZ_ERROR(ME, "Cannot get " SM_DM_DU_DUID);
    }

    return ret;
}

amxd_object_t* get_ee_obj_from_keys(amxd_dm_t* dm, amxc_var_t* args) {
    amxd_object_t* ret = NULL;
    const char* name = NULL;

    // Get Unique keys
    name = GET_CHAR(args, SM_DM_EE_NAME);
    if(name) {
        ret = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EE ".[" SM_DM_EE_NAME " == '%s'].", name);
    } else {
        SAH_TRACEZ_ERROR(ME, "Cannot get " SM_DM_EE_NAME);
    }

    return ret;
}

char* generate_duid(const char* uuid, const char* eeref) {
    uuid_t uuid_ns;
    char* ret = NULL;
    uuid_t uuid_ret;
    size_t eeref_len;

    if(uuid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing uuid");
        return NULL;
    }

    if(eeref == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing eeref");
        return NULL;
    }

    if(uuid_parse(uuid, uuid_ns)) {
        SAH_TRACEZ_ERROR(ME, "'%s' is not a valid UUID", uuid);
        return NULL;
    }

    SAH_TRACEZ_INFO(ME, "Generating duid for " SM_DM_DU_UUID ": '%s' and eeref: '%s'", uuid, eeref);

    eeref_len = strlen(eeref);
    uuid_generate_sha1(uuid_ret, uuid_ns, eeref, eeref_len);

    ret = malloc(UUID_STR_LEN);
    if(ret == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot allocate buffer for " SM_DM_DU_DUID);
        return NULL;
    }

    uuid_unparse_lower(uuid_ret, ret);

    SAH_TRACEZ_INFO(ME, "Generated " SM_DM_DU_DUID ": '%s'", ret);

    return ret;
}

bool string_starts_with_device_prefix(const char* value) {
    int ret = memcmp(value, DEVICE_DM_PREFIX, sizeof(DEVICE_DM_PREFIX) - 1);
    if(ret == 0) {
        return true;
    }
    return false;
}

bool string_starts_with_sm_prefix(const char* value) {
    int ret = memcmp(value, SOFTWAREMODULES_DM ".", sizeof(SOFTWAREMODULES_DM ".") - 1);
    if(ret == 0) {
        return true;
    }
    return false;
}

char* get_object_devices_path(amxd_object_t* obj, int flags) {
    char* path = NULL;
    const char* objpath = amxd_object_get_name(obj, flags);
    if(objpath) {
        unsigned int pathsize = sizeof(DEVICE_DM_PREFIX SOFTWAREMODULES_DM_EU ".") + strlen(objpath);
        path = (char*) calloc(1, pathsize);
        if(path && ((pathsize - 1) != (unsigned int) snprintf(path, pathsize, DEVICE_DM_PREFIX SOFTWAREMODULES_DM_EU ".%s", objpath))) {
            SAH_TRACEZ_ERROR(ME, "Failed to create Device. prefixed path");
            free(path);
            path = NULL;
        }
    }
    return path;
}

