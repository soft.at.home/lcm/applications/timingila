include ../makefile.inc

# build destination directories
OBJDIR = ../output/$(MACHINE)

# TARGETS
TARGET = $(OBJDIR)/$(COMPONENT).so


# directories
# source directories
SRCDIR = $(shell pwd)
INCDIR_PUBL = $(SRCDIR)/../include
INCDIR_PRIV = $(SRCDIR)/../include_priv
INCDIRS = $(INCDIR_PRIV) $(INCDIR_PUBL) $(INCDIR_LIBOCISPEC) $(if $(STAGINGDIR), $(STAGINGDIR)/include $(STAGINGDIR)/usr/include)
STAGING_LIBDIR = $(if $(STAGINGDIR), -L$(STAGINGDIR)/lib -L$(STAGINGDIR)/usr/lib)

# files
SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES:.c=.o)))

CFLAGS +=  -fPIC
# compilation and linking flags
CFLAGS_TIMINGILA = $(CFLAGS) -Werror -Wall -Wextra \
          -Wformat=2 -Wshadow -Wwrite-strings \
          -Wredundant-decls \
          -Wmissing-declarations -Wno-attributes \
          -Wno-format-nonliteral \
          -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
          -fPIC -g3 $(addprefix -I ,$(INCDIRS))

ifeq ($(CC_NAME),g++)
    CFLAGS_TIMINGILA += -std=c++2a
else
    CFLAGS_TIMINGILA += -Wstrict-prototypes -Wold-style-definition -Wnested-externs -std=c11
endif

ifeq ($(CONFIG_SAH_SERVICES_TIMINGILA_INIT_ADAPTERS),y)
  CFLAGS_TIMINGILA_ADAPTERS = -DTIMINGILA_INIT_ADAPTERS=1
  CFLAGS_TIMINGILA_ADAPTERS += -D'TIMINGILA_PATH_SO_CONTAINER="$(CONFIG_SAH_SERVICES_TIMINGILA_ADAPTER_CONTAINER)"'
  CFLAGS_TIMINGILA_ADAPTERS += -D'TIMINGILA_PATH_SO_PACKAGER="$(CONFIG_SAH_SERVICES_TIMINGILA_ADAPTER_PACKAGER)"'
endif
CFLAGS_TIMINGILA_ADAPTERS ?=
CFLAGS_TIMINGILA += $(CFLAGS_TIMINGILA_ADAPTERS)

LDFLAGS += -fPIC

LDFLAGS_TIMINGILA = $(LDFLAGS) $(STAGING_LIBDIR) \
           -shared \
           -lamxc -lamxp -lamxd \
           -lamxo -lamxm -lsahtrace \
           -luuid -llcm

# targets
all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) -Wl,-soname,$(TARGET) -o $@ $(OBJECTS) $(LDFLAGS_TIMINGILA)

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS_TIMINGILA) -c -o $@ $<
	@$(CC) $(CFLAGS_TIMINGILA) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	$(MKDIR) -p $@

clean:
	rm -rf ../output/


.PHONY: all clean
