/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <sys/time.h>
#include <sys/resource.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <inttypes.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include <timingila/timingila_defines.h>
#include <debug/sahtrace.h>
#include "timingila_priv.h"
#include "softwaremodules_priv.h"
#include "test_softwaremodules.h"

#include "test_setup.h"

#define UNUSED __attribute__((unused))

#define TEST_UUID "00000000-0971-52b1-b70c-47ab6ce48227"

// static void init_trace() {
//     sahTraceSetLevel(500);
//     sahTraceAddZone(500, "softwaremodules_du");
//     sahTraceAddZone(500, "softwaremodules_ee");
//     sahTraceAddZone(500, "softwaremodules_eu");
//     sahTraceAddZone(500, "rlyeh");
//     sahTraceOpen("SM_TEST", TRACE_TYPE_STDOUT);
//     SAH_TRACEZ_INFO("Generic", "sahTrace initialized");
// }

// static void clean_trace() {
//     sahTraceClose();
// }

void test_uuid(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_init(&args);
    const char* operation = "test";

    assert_int_equal(check_uuid(NULL, operation, &args, NULL), amxd_status_invalid_arg);
    assert_int_equal(check_uuid("", operation, &args, NULL), amxd_status_invalid_arg);
    // wrong length
    assert_int_equal(check_uuid("11715ea7-0971-52b1-b70c-47ab6ce4822", operation, &args, NULL), amxd_status_invalid_arg);
    // missing hyphen
    assert_int_equal(check_uuid("11715ea700971-52b1-b70c-47ab6ce48227", operation, &args, NULL), amxd_status_invalid_arg);
    // wrong type
    assert_int_equal(check_uuid("11715ea7-0971-42b1-b70c-47ab6ce48227", operation, &args, NULL), amxd_status_invalid_arg);
    // wrong N value
    assert_int_equal(check_uuid("11715ea7-0971-52b1-c70c-47ab6ce48227", operation, &args, NULL), amxd_status_invalid_arg);
    // wrong N value
    assert_int_equal(check_uuid("11715ea7-0971-52b1-C70c-47ab6ce48227", operation, &args, NULL), amxd_status_invalid_arg);
    // wrong N value
    assert_int_equal(check_uuid("11715ea7-0971-52b1-170c-47ab6ce48227", operation, &args, NULL), amxd_status_invalid_arg);
    // last digit wrong
    assert_int_equal(check_uuid("11715ea7-0971-52b1-b70c-47ab6ce4822J", operation, &args, NULL), amxd_status_invalid_arg);
    assert_int_equal(check_uuid("11715ea7-0971-52b1-870c-47ab6ce48227", operation, &args, NULL), amxd_status_ok);
    assert_int_equal(check_uuid("11715ea7-0971-52b1-970c-47ab6ce48227", operation, &args, NULL), amxd_status_ok);
    assert_int_equal(check_uuid("11715ea7-0971-52b1-a70c-47ab6ce48227", operation, &args, NULL), amxd_status_ok);
    assert_int_equal(check_uuid("11715ea7-0971-52b1-A70c-47ab6ce48227", operation, &args, NULL), amxd_status_ok);
    assert_int_equal(check_uuid("11715ea7-0971-52b1-b70c-47ab6ce48227", operation, &args, NULL), amxd_status_ok);
    assert_int_equal(check_uuid("11715EA7-0971-52B1-B70C-47AB6CE48227", operation, &args, NULL), amxd_status_ok);

    amxc_var_clean(&args);
}

void test_install_du(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM);
    amxc_var_t args;
    amxc_var_t ret;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_URL, "url");
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_UUID, "10000000-0971-52b1-b70c-47ab6ce48227");
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_EE_REF, "generic");
    assert_int_equal(amxd_object_invoke_function(dm_obj, "InstallDU", &args, &ret), amxd_status_deferred);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}

void test_add_exec_env(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM);
    amxc_var_t args;
    amxc_var_t ret;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, SM_DM_EE_NAME, "name");
    amxc_var_add_key(cstring_t, &args, SM_DM_EE_VENDOR, "vendor");
    amxc_var_add_key(cstring_t, &args, SM_DM_EE_VERSION, "version");
    amxc_var_add_key(cstring_t, &args, SM_DM_EE_PARENT_EE, "parentexecenv");
    amxc_var_add_key(int16_t, &args, SM_DM_EE_INITRUNLEVEL, 1);
    amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCMEM, 1);
    amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCDISKSPACE, 1);
    amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCCPUPERCENT, 1);
    assert_int_equal(amxd_object_invoke_function(dm_obj, "AddExecEnv", &args, &ret), amxd_status_deferred);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}

void test_update_du(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM_DU ".cpe-" TEST_UUID);
    amxc_var_t args;
    amxc_var_t ret;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_URL, "url");
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_USERNAME, "username");
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_PASSWORD, "password");
    assert_int_equal(amxd_object_invoke_function(dm_obj, "Update", &args, &ret), amxd_status_deferred);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}
void test_uninstall_du(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM_DU ".cpe-" TEST_UUID);
    amxc_var_t args;
    amxc_var_t ret;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(dm_obj, "Uninstall", &args, &ret), amxd_status_deferred);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}

void test_ee_restart(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM_EE ".cpe-generic");
    amxc_var_t args;
    amxc_var_t ret;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, SM_DM_EE_RESTART_REASON, "reason");
    assert_int_equal(amxd_object_invoke_function(dm_obj, "Restart", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}

void test_ee_delete(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM_EE ".cpe-generic");
    amxc_var_t args;
    amxc_var_t ret;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(dm_obj, "Delete", &args, &ret), amxd_status_invalid_action);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}

void test_ee_modifyconstraints(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM_EE ".cpe-generic");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* free_var = NULL;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_not_equal(amxd_object_invoke_function(dm_obj, "ModifyConstraints", &args, &ret), amxd_status_ok);
    amxc_var_add_key(bool, &args, SM_DM_EE_ARG_FORCE, false);
    assert_int_not_equal(amxd_object_invoke_function(dm_obj, "ModifyConstraints", &args, &ret), amxd_status_ok);
    amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCCPUPERCENT, -2);
    assert_int_not_equal(amxd_object_invoke_function(dm_obj, "ModifyConstraints", &args, &ret), amxd_status_ok);
    free_var = amxc_var_take_key(&args, SM_DM_EE_ALLOCCPUPERCENT);
    amxc_var_delete(&free_var);
    amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCCPUPERCENT, 101);
    assert_int_not_equal(amxd_object_invoke_function(dm_obj, "ModifyConstraints", &args, &ret), amxd_status_ok);
    free_var = amxc_var_take_key(&args, SM_DM_EE_ALLOCCPUPERCENT);
    amxc_var_delete(&free_var);
    amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCMEM, -2);
    assert_int_not_equal(amxd_object_invoke_function(dm_obj, "ModifyConstraints", &args, &ret), amxd_status_ok);
    free_var = amxc_var_take_key(&args, SM_DM_EE_ALLOCMEM);
    amxc_var_delete(&free_var);
    amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCDISKSPACE, -2);
    assert_int_not_equal(amxd_object_invoke_function(dm_obj, "ModifyConstraints", &args, &ret), amxd_status_ok);
    /*
     * amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCCPUPERCENT, -1);
     * amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCDISKSPACE, -1);
     * amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCMEM, -1);
     *
     * assert_int_equal(amxd_object_invoke_function(dm_obj, "ModifyConstraints", &args, &ret), amxd_status_ok);
     * amxc_var_take_key(&args, SM_DM_EE_ALLOCCPUPERCENT);
     * amxc_var_take_key(&args, SM_DM_EE_ALLOCDISKSPACE);
     * amxc_var_take_key(&args, SM_DM_EE_ALLOCMEM);
     * amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCCPUPERCENT, 100);
     * amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCDISKSPACE, 0x7FFFFFFF);
     * amxc_var_add_key(int32_t, &args, SM_DM_EE_ALLOCMEM, 0x7FFFFFFF);
     * assert_int_equal(amxd_object_invoke_function(dm_obj, "ModifyConstraints", &args, &ret), amxd_status_ok);
     */
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}

void test_ee_setrunlevel(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM_EE ".cpe-generic");
    amxc_var_t args;
    amxc_var_t ret;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int32_t, &args, "RequestedRunLevel", 5);
    assert_int_equal(amxd_object_invoke_function(dm_obj, "SetRunLevel", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}

void test_eu_setrequestedstate(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM_EU ".cpe-test-eu");
    amxc_var_t args;
    amxc_var_t ret;

    // init_trace();

    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, SM_DM_EU_REQSTATE, "state");
    assert_int_equal(amxd_object_invoke_function(dm_obj, "SetRequestedState", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // clean_trace();
}

void test_string_starts_with_device_prefix(UNUSED void** state) {
    char proper_device_prefix_string[sizeof(DEVICE_DM_PREFIX)] = DEVICE_DM_PREFIX;
    char unproper_device_prefix_string[4] = {0};
    bool ret;

    memcpy(unproper_device_prefix_string, proper_device_prefix_string, 3);

    ret = string_starts_with_device_prefix(proper_device_prefix_string);
    assert_true(ret);

    proper_device_prefix_string[sizeof(DEVICE_DM_PREFIX) - 2] = '\0';
    ret = string_starts_with_device_prefix(proper_device_prefix_string);
    assert_false(ret);

    proper_device_prefix_string[0] = '\0';
    ret = string_starts_with_device_prefix(proper_device_prefix_string);
    assert_false(ret);

    ret = string_starts_with_device_prefix(unproper_device_prefix_string);
    assert_false(ret);
}

void test_string_starts_with_sm_prefix(UNUSED void** state) {
    char proper_sm_prefix_string[sizeof(SOFTWAREMODULES_DM ".")] = SOFTWAREMODULES_DM ".";
    char unproper_sm_prefix_string[4] = {0};
    bool ret;

    memcpy(unproper_sm_prefix_string, proper_sm_prefix_string, 3);

    ret = string_starts_with_sm_prefix(proper_sm_prefix_string);
    assert_true(ret);

    proper_sm_prefix_string[sizeof(SOFTWAREMODULES_DM ".") - 2] = '\0';
    ret = string_starts_with_sm_prefix(proper_sm_prefix_string);
    assert_false(ret);

    proper_sm_prefix_string[0] = '\0';
    ret = string_starts_with_sm_prefix(proper_sm_prefix_string);
    assert_false(ret);

    ret = string_starts_with_sm_prefix(unproper_sm_prefix_string);
    assert_false(ret);
}
