/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <debug/sahtrace.h>
#include <stdlib.h>

#include "timingila/timingila_defines.h"

#include "dmmngr_priv.h"
#include "timingila_priv.h"

#define ME TIMINGILA_MOD_DM_MNGR

static amxm_module_t* mod_dm_mngr = NULL;

static const char* amxd_status_names[] = {
    "amxd_status_ok",
    "amxd_status_unknown_error",
    "amxd_status_object_not_found",
    "amxd_status_function_not_found",
    "amxd_status_parameter_not_found",
    "amxd_status_function_not_implemented",
    "amxd_status_invalid_function",
    "amxd_status_invalid_function_argument",
    "amxd_status_invalid_name",
    "amxd_status_invalid_attr",
    "amxd_status_invalid_value",
    "amxd_status_invalid_action",
    "amxd_status_invalid_type",
    "amxd_status_duplicate",
    "amxd_status_deferred",
    "amxd_status_read_only",
    "amxd_status_missing_key",
    "amxd_status_file_not_found",
    "amxd_status_invalid_arg",
    "amxd_status_out_of_mem",
    "amxd_status_recursion",
    "amxd_status_invalid_path",
    "amxd_status_invalid_expr",
    "amxd_status_permission_denied",
    "amxd_status_not_supported"
};

static const char* amxd_status_to_string(amxd_status_t status) {
    if((status >= 0) && (status < amxd_status_last)) {
        return amxd_status_names[status];
    } else {
        return NULL;
    }
}

static amxd_status_t timingila_dm_mngr_set_parameters(amxd_trans_t* transaction, amxc_var_t* parameters) {
    amxd_status_t status = amxd_status_ok;
    amxc_var_for_each(param, parameters) {
        const char* param_name = amxc_var_key(param);
        if(amxc_var_type_of(param) == AMXC_VAR_ID_HTABLE) {
            continue;
        }
        status = amxd_trans_set_param(transaction, param_name, param);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Could not set param %s. %s [%d]", param_name, amxd_status_to_string(status), status);
            goto exit;
        }
    }

exit:
    return status;
}

static amxd_status_t timingila_dm_mngr_object_set_child_parameters(amxd_object_t* object, amxc_var_t* parameters) {
    amxd_status_t status = amxd_status_ok;

    // apply settings on the children
    amxc_var_for_each(param, parameters) {
        if(amxc_var_type_of(param) != AMXC_VAR_ID_HTABLE) {
            continue;
        }
        amxd_trans_t transaction;
        amxd_trans_init(&transaction);
        amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
        const char* key = amxc_var_key(param);
        // get child
        amxd_object_t* child = amxd_object_findf(object, "%s", key);
        if(child) {
            amxd_trans_select_object(&transaction, child);
        } else {
            // create instance if child does not exist
            if(amxd_object_get_type(object) != amxd_object_template) {
                SAH_TRACEZ_ERROR(ME, "Object  %s is not a template, cannot add an instance", amxd_object_get_path(object, AMXD_OBJECT_NAMED));
                status = amxd_status_invalid_type;
                amxd_trans_clean(&transaction);
                goto exit;
            }
            amxd_trans_select_object(&transaction, object);
            // get index out of the key, so updates happen on the correct object
            int index = atoi(key);
            amxd_trans_add_inst(&transaction, index, NULL);
        }
        status = timingila_dm_mngr_set_parameters(&transaction, param);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Setting parameters failed: %s [%d]", amxd_status_to_string(status), status);
            amxd_trans_clean(&transaction);
            goto exit;
        }
        status = amxd_trans_apply(&transaction, timingila_get_dm());
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Transaction failed: %s [%d]", amxd_status_to_string(status), status);
            amxd_trans_clean(&transaction);
            goto exit;
        }

        amxd_trans_clean(&transaction);
        if(!child) {
            child = amxd_object_findf(object, "%s", key);
            if(!child) {
                SAH_TRACEZ_ERROR(ME, "Could not find created instance");
                status = amxd_status_object_not_found;
                amxd_trans_clean(&transaction);
                goto exit;
            }
        }
        status = timingila_dm_mngr_object_set_child_parameters(child, param);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Could not set child params: %s [%d]", amxd_status_to_string(status), status);
            goto exit;
        }
    }
    // remove instances that disappeared
    if(amxd_object_get_type(object) == amxd_object_template) {
        amxd_object_for_each(instance, it, object) {
            bool idx_found = false;
            amxd_object_t* inst = amxc_llist_it_get_data(it, amxd_object_t, it);
            uint32_t idx = amxd_object_get_index(inst);
            amxc_var_for_each(param, parameters) {
                if(amxc_var_type_of(param) != AMXC_VAR_ID_HTABLE) {
                    continue;
                }
                uint32_t param_idx = atoi(amxc_var_key(param));
                if(idx == param_idx) {
                    idx_found = true;
                    break;
                }
            }
            if(!idx_found) {
                timingila_dm_mngr_remove(&inst);
            }
        }
    }


exit:
    return status;
}

int timingila_dm_mngr_create(amxd_object_t* object,
                             amxc_var_t* parameters,
                             amxc_var_t* ret) {
    int retval = -1;
    amxd_dm_t* dm = timingila_get_dm();
    amxd_trans_t transaction;

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&transaction, object);
    amxd_trans_add_inst(&transaction, 0, NULL);
    timingila_dm_mngr_set_parameters(&transaction, parameters);

    amxd_status_t status = amxd_trans_apply(&transaction, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed: %s [%d]", amxd_status_to_string(status), status);
        goto exit;
    }
    amxc_var_set(uint32_t, ret, amxd_object_get_index(transaction.current));

    timingila_dm_mngr_object_set_child_parameters(transaction.current, parameters);

    retval = 0;
exit:
    amxd_trans_clean(&transaction);
    return retval;
}

int timingila_dm_mngr_update(amxd_object_t* object,
                             amxc_var_t* parameters) {
    int retval = -1;
    amxd_dm_t* dm = timingila_get_dm();
    amxd_trans_t transaction;

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&transaction, object);
    timingila_dm_mngr_set_parameters(&transaction, parameters);

    amxd_status_t status = amxd_trans_apply(&transaction, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed: %s [%d]", amxd_status_to_string(status), status);
        goto exit;
    }

    timingila_dm_mngr_object_set_child_parameters(object, parameters);

    retval = 0;
exit:
    amxd_trans_clean(&transaction);
    return retval;
}

int timingila_dm_mngr_remove(amxd_object_t** object) {
    int retval = -1;
    if((object == NULL) || (*object == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Cannot delete object");
        goto exit;
    }

    // workaround for not using a transaction
    amxd_object_emit_del_inst(*object);
    amxd_object_delete(object);
    retval = 0;
exit:
    return retval;
}

int timingila_dm_mngr_add_function(const char* func_name, amxm_callback_t cb) {
    int retval = -1;

    when_str_empty_log(func_name, exit);
    when_null_log(cb, exit);

    SAH_TRACEZ_INFO(ME, "Adding function: %s, %s, %s", "self", TIMINGILA_MOD_DM_MNGR, func_name);
    retval = amxm_module_add_function(mod_dm_mngr, func_name, cb);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldn't add function: '%s'", func_name);
    }
exit:
    return retval;
}

int timingila_dm_mngr_init(void) {
    int retval = -1;
    amxm_shared_object_t* so = amxm_get_so(AMXM_SELF);
    if(so == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot get " AMXM_SELF);
        goto exit;
    }

    if(amxm_module_register(&mod_dm_mngr, so, TIMINGILA_MOD_DM_MNGR)) {
        SAH_TRACEZ_ERROR(ME, "Cannot register module");
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}
