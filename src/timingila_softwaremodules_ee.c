/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>

#include <lcm/lcm_dump_rpc.h>
#include "timingila/timingila_defines.h"

#include "softwaremodules_priv.h"
#include "timingila_priv.h"

#define ME "softwaremodules_ee"

static inline amxd_object_t* get_ee(const char* name) {
    amxd_object_t* ee = NULL;
    amxd_dm_t* dm = timingila_get_dm();
    ee = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EE ".[" SM_DM_EE_NAME " == '%s'].", name);
    return ee;
}

static bool _container_ee_stats(const char* ee_alias, const char* ee_stat_name, amxc_var_t* const retval) {
    bool ret;
    amxc_var_t params;
    amxc_var_init(&params);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, SM_DM_EE_ALIAS, ee_alias);
    amxc_var_add_key(cstring_t, &params, SM_EE_STAT_NAME, ee_stat_name);

    ret = container_ee_stats(&params, retval);
    amxc_var_clean(&params);
    return ret;
}

static amxd_status_t recursive_delete_child_ee_and_dus(amxd_object_t* object) {
    amxd_status_t status = amxd_status_unknown_error;
    bool retb = false;
    amxc_var_t params;
    char* execenvname = NULL;
    amxd_dm_t* dm = timingila_get_dm();
    amxd_object_t* execenv = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EE);

    when_null(object, exit);

    if(execenv == NULL) {
        SAH_TRACEZ_ERROR(ME, "Can't find " SOFTWAREMODULES_DM_EE);
        status = amxd_status_object_not_found;
        goto exit;
    }

    execenvname = amxd_object_get_cstring_t(object, SM_DM_EE_NAME, NULL);

    SAH_TRACEZ_INFO(ME, "Delete Exec Env [%s]", execenvname);

    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_public);

    amxd_object_for_each(instance, it, execenv) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* parent_ee = amxd_object_get_cstring_t(instance, SM_DM_EE_PARENT_EE, NULL);
        // char* ee_name = amxd_object_get_cstring_t(instance, SM_DM_EE_NAME, NULL);
        if(!string_is_empty(parent_ee) && (strcmp(parent_ee, execenvname) == 0)) {
            amxc_var_t arg;
            amxc_var_init(&arg);
            amxc_var_set_type(&arg, AMXC_VAR_ID_HTABLE);
            amxd_status_t retval = recursive_delete_child_ee_and_dus(instance);
            if(retval != amxd_status_ok) {
                SAH_TRACEZ_ERROR(ME, "delete_ee_and_du returned an error");
            }
            amxc_var_clean(&arg);
        }
        free(parent_ee);
    }

    amxd_object_t* deployment_unit = amxd_dm_findf(dm, SOFTWAREMODULES_DM_DU);
    amxd_object_for_each(instance, it, deployment_unit) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* execenvref = amxd_object_get_cstring_t(instance, SM_DM_DU_EE_REF, NULL);
        char* duname = amxd_object_get_cstring_t(instance, SM_DM_DU_NAME, NULL);
        if(!string_is_empty(execenvref) && (strcmp(execenvref, execenvname) == 0)) {
            amxc_var_t ret_rc;
            amxc_var_t arg;
            amxc_var_init(&ret_rc);
            amxc_var_init(&arg);
            amxc_var_set_type(&arg, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(bool, &arg, "RetainData", false);
            amxd_object_invoke_function(instance, "Uninstall", &arg, &ret_rc);
            amxc_var_clean(&arg);
            amxc_var_clean(&ret_rc);
        }
        free(duname);
        free(execenvref);
    }

    retb = container_ee_delete(&params);
    if(!retb) {
        SAH_TRACEZ_ERROR(ME, "Couldn't delete ee %s", execenvname);
    }

    status = amxd_status_ok;

exit:
    amxc_var_clean(&params);
    return status;
}

amxd_status_t _ExecEnv_SetRunLevel(amxd_object_t* object,
                                   UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t params;
    const int32_t requestedrunlevel = GET_INT32(args, "RequestedRunLevel");

    SAH_TRACEZ_ERROR(ME, "RequestedRunLevel: %d", requestedrunlevel);

    when_null(object, exit);
    amxc_var_init(&params);

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    SAH_TRACEZ_ERROR(ME, SM_DM_EE_ALIAS ": %s", GET_CHAR(&params, SM_DM_EE_ALIAS));
    SAH_TRACEZ_ERROR(ME, SM_DM_EE_NAME ": %s", GET_CHAR(&params, SM_DM_EE_NAME));

    // TODO: set runlevel on ee

    status = amxd_status_ok;
    amxc_var_clean(&params);
exit:
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

amxd_status_t _SoftwareModules_AddExecEnv(UNUSED amxd_object_t* object,
                                          UNUSED amxd_function_t* func,
                                          amxc_var_t* args,
                                          amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;
    bool retb = false;
    uint64_t callid;
    const char* ee_name = GET_CHAR(args, SM_DM_EE_NAME);

    SAH_TRACEZ_NOTICE(ME, "_SoftwareModules_AddExecEnv " SM_DM_EE_NAME ": %s", ee_name);

    if(get_ee(ee_name) != NULL) {
        SAH_TRACEZ_ERROR(ME, "Couldn't add EE with duplicate " SM_DM_EE_NAME ": %s", ee_name);
        amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_USP_INTERNAL_ERROR);
        amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, "An ee with the same " SM_DM_EE_NAME " already exists");
        status = amxd_status_invalid_arg;
        goto exit;
    }

    amxd_function_defer(func, &callid, ret, NULL, NULL);
    amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_CALLID, callid);

    retb = container_ee_add(args);
    if(!retb) {
        SAH_TRACEZ_ERROR(ME, "Couldn't add exec env");
        amxd_function_deferred_remove(callid);
        amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_USP_INTERNAL_ERROR);
        // This can be that the module (packager) is not loaded or,
        // the package is not happy with the arguments provided (and thus returns)
        // or the object is missing in this call
        amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_USP_INTERNAL_ERROR_STR);
        goto exit;
    }

    status = amxd_status_deferred;
exit:
    return status;
}

amxd_status_t _ExecEnv_Restart(amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               UNUSED amxc_var_t* args,
                               amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;
    bool retb = false;
    amxc_var_t params;

    when_null(object, exit);
    amxc_var_init(&params);

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    // replace RestartReason
    amxc_var_t* ptr = amxc_var_take_key(&params, SM_DM_EE_RESTART_REASON);
    if(ptr) {
        amxc_var_delete(&ptr);
    }
    amxc_var_add_key(cstring_t, &params, SM_DM_EE_RESTART_REASON, GET_CHAR(args, SM_DM_EE_RESTART_REASON));

    SAH_TRACEZ_NOTICE(ME, SM_DM_EE_ALIAS ": %s", GET_CHAR(&params, SM_DM_EE_ALIAS));
    SAH_TRACEZ_NOTICE(ME, SM_DM_EE_NAME ": %s", GET_CHAR(&params, SM_DM_EE_NAME));
    SAH_TRACEZ_NOTICE(ME, SM_DM_EE_RESTART_REASON ": %s", GET_CHAR(&params, SM_DM_EE_RESTART_REASON));

    retb = container_ee_restart(&params);
    if(!retb) {
        SAH_TRACEZ_ERROR(ME, "Couldn't restart ee");
    }

    status = amxd_status_ok;
    amxc_var_clean(&params);

exit:
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

amxd_status_t _ExecEnv_ModifyConstraints(amxd_object_t* object,
                                         UNUSED amxd_function_t* func,
                                         amxc_var_t* args,
                                         amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, NULL);

    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t params;
    amxc_var_t* alloc_cpu_var = GET_ARG(args, SM_DM_EE_ALLOCCPUPERCENT);
    int32_t allocated_cpu_percent = GET_INT32(args, SM_DM_EE_ALLOCCPUPERCENT);
    amxc_var_t* alloc_mem_var = GET_ARG(args, SM_DM_EE_ALLOCMEM);
    int32_t allocated_memory = GET_INT32(args, SM_DM_EE_ALLOCMEM);
    amxc_var_t* alloc_diskspace_var = GET_ARG(args, SM_DM_EE_ALLOCDISKSPACE);
    int32_t allocated_diskspace = GET_INT32(args, SM_DM_EE_ALLOCDISKSPACE);
    // returns false when the SM_DM_EE_ARG_FORCE is not there
    bool force = GET_BOOL(args, SM_DM_EE_ARG_FORCE);
    amxc_var_t* free_var = NULL;

    when_null(object, exit);

    if((alloc_cpu_var == NULL) && (alloc_mem_var == NULL) && (alloc_diskspace_var == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Need atleast " SM_DM_EE_ALLOCCPUPERCENT " or " SM_DM_EE_ALLOCMEM " or " SM_DM_EE_ALLOCDISKSPACE);
        status = amxd_status_invalid_arg;
        goto exit;
    }

    if(alloc_cpu_var) {
        if(allocated_cpu_percent < -1) {
            SAH_TRACEZ_ERROR(ME, SM_DM_EE_ALLOCCPUPERCENT " cannot be smaller then -1");
            status = amxd_status_invalid_arg;
            goto exit;
        }
        if(allocated_cpu_percent > 100) {
            SAH_TRACEZ_ERROR(ME, SM_DM_EE_ALLOCCPUPERCENT " cannot be bigger then 100");
            status = amxd_status_invalid_arg;
            goto exit;
        }
    }

    if(alloc_mem_var) {
        if(allocated_memory < -1) {
            SAH_TRACEZ_ERROR(ME, SM_DM_EE_ALLOCMEM " cannot be smaller then -1");
            status = amxd_status_invalid_arg;
            goto exit;
        }
    }

    if(alloc_diskspace_var) {
        if(allocated_diskspace < -1) {
            SAH_TRACEZ_ERROR(ME, SM_DM_EE_ALLOCDISKSPACE " cannot be smaller then -1");
            status = amxd_status_invalid_arg;
            goto exit;
        }
    }

    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_public);

    // Add args
    amxc_var_add_key(bool, &params, SM_DM_EE_ARG_FORCE, force);

    // Change args because we copied dm
    free_var = amxc_var_take_key(&params, SM_DM_EE_ALLOCCPUPERCENT);
    amxc_var_delete(&free_var);
    if(alloc_cpu_var) {
        amxc_var_add_key(int32_t, &params, SM_DM_EE_ALLOCCPUPERCENT, allocated_cpu_percent);
    }

    free_var = amxc_var_take_key(&params, SM_DM_EE_ALLOCMEM);
    amxc_var_delete(&free_var);
    if(alloc_mem_var) {
        amxc_var_add_key(int32_t, &params, SM_DM_EE_ALLOCMEM, allocated_memory);
    }

    free_var = amxc_var_take_key(&params, SM_DM_EE_ALLOCDISKSPACE);
    amxc_var_delete(&free_var);
    if(alloc_diskspace_var) {
        amxc_var_add_key(int32_t, &params, SM_DM_EE_ALLOCDISKSPACE, allocated_diskspace);
    }

    // the parameters that are not to be changed are removed from the params
    if(container_ee_modifyconstraints(&params)) {
        status = amxd_status_ok;
    } else {
        SAH_TRACEZ_ERROR(ME, "Couldn't modify the constraints of the ee");
    }

    amxc_var_clean(&params);
exit:
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

amxd_status_t _ExecEnv_ModifyAvailableRoles(amxd_object_t* object,
                                            amxd_function_t* func,
                                            amxc_var_t* args,
                                            UNUSED amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, NULL);

    uint64_t callid;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t params;
    amxc_var_t* available_roles_from_dm = NULL;

    amxc_var_t* available_roles = GET_ARG(args, SM_DM_EE_AVAILABLE_ROLES);
    when_null(available_roles, exit);

    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_public);

    amxd_function_defer(func, &callid, ret, NULL, NULL);

    // Offcourse parameter has the same name :D jippie -> remove it to have the new
    available_roles_from_dm = amxc_var_take_key(&params, SM_DM_EE_AVAILABLE_ROLES);
    amxc_var_delete(&available_roles_from_dm);
    amxc_var_set_key(&params, SM_DM_EE_AVAILABLE_ROLES, available_roles, AMXC_VAR_FLAG_COPY);
    amxc_var_add_key(uint64_t, &params, AMXB_DEFERRED_CALLID, callid);

    if(container_ee_modifyavailableroles(&params)) {
        status = amxd_status_deferred;
    } else {
        amxd_function_deferred_remove(callid);
        amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_USP_INTERNAL_ERROR);
        // This can be that the module (packager) is not loaded or,
        // the package is not happy with the arguments provided (and thus returns)
        // or the object is missing in this call
        amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_USP_INTERNAL_ERROR_STR);
    }

    amxc_var_clean(&params);
exit:
    return status;
}

amxd_status_t _ExecEnv_Delete(amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              UNUSED amxc_var_t* args,
                              amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, NULL);

    amxd_status_t status = amxd_status_unknown_error;
    bool retb = false;
    amxc_var_t params;
    char* execenvname = NULL;

    when_null(object, exit);

    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_public);

    execenvname = amxd_object_get_cstring_t(object, SM_DM_EE_NAME, NULL);

    amxd_dm_t* dm = timingila_get_dm();
    amxd_object_t* deployment_unit = amxd_dm_findf(dm, SOFTWAREMODULES_DM_DU);
    amxd_object_for_each(instance, it, deployment_unit) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* execenvref = amxd_object_get_cstring_t(instance, SM_DM_DU_EE_REF, NULL);
        if(!string_is_empty(execenvref) && (strcmp(execenvref, execenvname) == 0)) {
            status = amxd_status_invalid_action;
            SAH_TRACEZ_ERROR(ME, "ExecEnv \"%s\" has one or more DUs, please delete it first", execenvname);
            amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_USP_INTERNAL_ERROR);
            amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, "ExecEnv has one or more DUs, please delete it first");
            free(execenvref);
            goto exit;
            // Uninstall the DUs running inside the EE being removed
            // amxc_var_t ret_rc;
            // amxc_var_t arg;
            // amxc_var_init(&arg);
            // amxc_var_init(&ret_rc);
            // amxc_var_set_type(&arg, AMXC_VAR_ID_HTABLE);
            // amxc_var_add_key(bool, &arg, "RetainData", false);
            // amxd_object_invoke_function(instance, "Uninstall", &arg, &ret_rc);
            // amxc_var_clean(&arg);
            // amxc_var_clean(&ret_rc);
        }
    }

    amxd_object_t* execenv = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EE);
    amxd_object_for_each(instance, it, execenv) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* parent_ee = amxd_object_get_cstring_t(instance, SM_DM_EE_PARENT_EE, NULL);
        if(!string_is_empty(parent_ee) && (strcmp(parent_ee, execenvname) == 0)) {
            amxc_var_t arg;
            amxc_var_init(&arg);
            amxc_var_set_type(&arg, AMXC_VAR_ID_HTABLE);
            amxd_status_t retval = recursive_delete_child_ee_and_dus(instance);
            if(retval != amxd_status_ok) {
                SAH_TRACEZ_ERROR(ME, "recursive_delete_child_EE_and_DUs returned an error");
            }
            amxc_var_clean(&arg);
        }
        free(parent_ee);
    }


    retb = container_ee_delete(&params);
    if(!retb) {
        SAH_TRACEZ_ERROR(ME, "Couldn't delete ee %s", execenvname);
    }

    status = amxd_status_ok;

exit:
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    amxc_var_clean(&params);
    if(execenvname) {
        free(execenvname);
    }
    return status;
}

amxd_status_t _ee_write_description(UNUSED amxd_object_t* object,
                                    UNUSED amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval,
                                    UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t params;

    SAH_TRACEZ_INFO(ME, "_ee_write_description");

    amxc_var_init(&params);

    if(reason != action_param_write) {
        status = amxd_status_function_not_implemented;
        goto error;
    }

    // set params
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, SM_DM_EE_ALIAS, amxd_object_get_name(object, AMXD_OBJECT_NAMED));
    amxc_var_add_key(cstring_t, &params, SM_DM_EE_DESCRIPTION, amxc_var_get_const_cstring_t(args));

    if(!container_ee_description(&params)) {
        SAH_TRACEZ_ERROR(ME, "Couldnt write Description");
        goto error;
    }

    // set parameter in the dm
    amxc_var_convert(&param->value, args, amxc_var_type_of(&param->value));

    status = amxd_status_ok;
    goto exit;

error:
    SAH_TRACEZ_ERROR(ME, "Error: %d", status);
    amxc_var_set(int32_t, retval, -1);
exit:
    amxc_var_clean(&params);
    return status;
}

amxd_status_t _ee_read_stats(UNUSED amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    if(reason != action_param_read) {
        status = amxd_status_function_not_implemented;
        goto error;
    }

    if(_container_ee_stats(amxd_object_get_name(object, AMXD_OBJECT_NAMED), param->name, retval) == false) {
        SAH_TRACEZ_ERROR(ME, "Couldn't read %s", param->name);
        goto error;
    }

    status = amxd_status_ok;
    goto exit;
error:
    SAH_TRACEZ_ERROR(ME, "Error: %d", status);
    amxc_var_set(int32_t, retval, -1);
exit:
    return status;
}

static amxd_object_t* get_parent_ee(amxd_object_t* object) {
    amxd_object_t* parent = amxd_object_get_parent(object);
    if(!parent) {
        return NULL;
    }

    if((amxd_object_get_type(parent) == amxd_object_template) &&
       (strcmp(amxd_object_get_name(parent, AMXD_OBJECT_NAMED), SM_DM_EE) == 0)) {
        return object;
    }
    return get_parent_ee(parent);
}

amxd_status_t _ee_read_param(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    char* rel_path = NULL;
    amxc_var_t params;
    amxc_var_init(&params);
    ASSERT_EQUAL(reason, action_param_read, status = amxd_status_function_not_implemented; goto error);

    // get the base ExecEnv object
    amxd_object_t* parent_ee = get_parent_ee(object);
    ASSERT_NOT_NULL(parent_ee, status = amxd_status_object_not_found; goto error);

    rel_path = amxd_object_get_rel_path(object, parent_ee, AMXD_OBJECT_INDEXED);
    ASSERT_STR_NOT_EMPTY(rel_path, goto error, "Could not define relpath");

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, SM_DM_EE_ALIAS, amxd_object_get_name(parent_ee, AMXD_OBJECT_NAMED));
    amxc_var_add_key(cstring_t, &params, SM_EE_RELPATH, rel_path);
    amxc_var_add_key(cstring_t, &params, SM_EE_PARAM, param->name);

    ASSERT_TRUE(container_ee_read_param(&params, retval), goto error, "Could not read %s.%s", rel_path, param->name);

    status = amxd_status_ok;
    goto exit;
error:
    SAH_TRACEZ_ERROR(ME, "Error: %d", status);
    amxc_var_set(int32_t, retval, -1);
exit:
    amxc_var_clean(&params);
    free(rel_path);
    return status;
}

void _ee_change_enable(UNUSED const char* const sig_name,
                       const amxc_var_t* const data,
                       UNUSED void* const priv) {
    amxd_object_t* eu = amxd_dm_signal_get_object(timingila_get_dm(), data);
    bool retb = false;
    amxc_var_t params;

    SAH_TRACEZ_INFO(ME, "_ee_change_enable");

    if(eu == NULL) {
        SAH_TRACEZ_ERROR(ME, "Object not found");
        return;
    }

    amxc_var_init(&params);
    amxd_object_get_params(eu, &params, amxd_dm_access_public);

    retb = container_ee_setenable(&params);
    if(retb) {
        SAH_TRACEZ_ERROR(ME, "Couldn't set autostart");
    }

    amxc_var_clean(&params);
}

