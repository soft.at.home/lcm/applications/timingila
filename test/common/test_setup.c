/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>


#include <timingila/timingila_defines.h>

#include "softwaremodules_priv.h"
#include "timingila_priv.h"
#include "test_setup.h"

static amxd_dm_t dm;
static amxo_parser_t parser;

static amxm_module_t dummy;
static const char* odl_timingila_definitions = "../../odl-ambiorix/timingila_definition.odl";
static const char* odl_softwaremodules_definitions = "../../odl-ambiorix/softwaremodules_definition.odl";
static const char* odl_defaults = "../config/softwaremodules_defaults.odl";

amxd_dm_t* test_get_dm(void) {
    return &dm;
}

int test_softwaremodules_setup(UNUSED void** state) {
    amxd_object_t* sm_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    sm_obj = amxd_dm_get_root(&dm);
    assert_non_null(sm_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "InstallDU", AMXO_FUNC(_SoftwareModules_InstallDU)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "AddExecEnv", AMXO_FUNC(_SoftwareModules_AddExecEnv)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, SOFTWAREMODULES_DM_EE ".SetRunLevel", AMXO_FUNC(_ExecEnv_SetRunLevel)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, SOFTWAREMODULES_DM_EE ".Restart", AMXO_FUNC(_ExecEnv_Restart)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, SOFTWAREMODULES_DM_EE ".Delete", AMXO_FUNC(_ExecEnv_Delete)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, SOFTWAREMODULES_DM_EE ".ModifyConstraints", AMXO_FUNC(_ExecEnv_ModifyConstraints)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, SOFTWAREMODULES_DM_DU ".Update", AMXO_FUNC(_DeploymentUnit_Update)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, SOFTWAREMODULES_DM_DU ".Uninstall", AMXO_FUNC(_DeploymentUnit_Uninstall)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, SOFTWAREMODULES_DM_EU ".SetRequestedState", AMXO_FUNC(_ExecutionUnit_SetRequestedState)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_softwaremodules_definitions, sm_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defaults, sm_obj), 0);

    _timingila_main(0, &dm, &parser);

    return 0;
}

int test_softwaremodules_teardown(UNUSED void** state) {
    _timingila_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}
